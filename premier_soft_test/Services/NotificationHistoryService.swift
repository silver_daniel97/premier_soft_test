//
//  NotificationHistoryService.swift
//  premier_soft_test
//
//  Created by Daniel Silveira on 24/06/21.
//

import Foundation

class NotificationHistoryService {
    static let shared = NotificationHistoryService()
    
    let http = HTTPService.shared
    
    func fetchAllNotifications(cb: @escaping ([Notification]) -> Void) {
        if let url = URL(string: ApiRoutes.getAllNotifications) {
            http.request(url: url) { data, urlResponse, error in
                print("requesting at \(url.absoluteURL)")
                if let data = data {
                    let container = try? JSONDecoder().decode(NotificationContainer.self, from: data)
                    
                    cb(container?.notifications ?? [])
                    return
                }
            }
        } else {
            print("an error occured while ")
            cb([])
        }
    }
}
