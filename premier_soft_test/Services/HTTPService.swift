//
//  HTTPService.swift
//  premier_soft_test
//
//  Created by Daniel Silveira on 24/06/21.
//

import Foundation

class HTTPService {
    static let shared = HTTPService()
    
    func request(url: URL, cb: @escaping (Data?, URLResponse?, Error?) -> ()) {

        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            cb(data, response, error)
        }

        task.resume()
    }
}
