//
//  NotificationHistoryListTableViewController.swift
//  premier_soft_test
//
//  Created by Daniel Silveira on 24/06/21.
//

import Foundation
import UIKit

class NotificationHistoryListTableViewController: UIViewController {
    
    let notificationService = NotificationHistoryService.shared
    
    var readNotifications: [Notification] = [Notification]()
    var unreadNotifications: [Notification] = [Notification]()
    
    private var myTableView: UITableView!
    
    lazy var refreshControl: UIRefreshControl = {
        return UIRefreshControl()
    }()
    lazy var emptyLabel: UILabel = {
        let label = UILabel()
        label.text = "Sem notificações"
        label.textAlignment = .center
        label.font = label.font.withSize(25)
        return label
    }()
    
    let cellHeight: CGFloat = 80
    let headerSectionHeight: CGFloat = 30

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        self.refreshControl.beginRefreshing()
        reloadNotifications(onFinished: {
            [weak self] in
            DispatchQueue.main.async {
                [weak self] in
                self?.refreshControl.endRefreshing()
            }
        })
    }
    
    func setupView() {
        refreshControl.addTarget(self, action: #selector(self.refreshData(_:)), for: .valueChanged)
        let screenFrame = view.frame
        myTableView = UITableView(frame: CGRect(x: 0, y: 0, width: screenFrame.width, height: screenFrame.height))
        myTableView.register(NotificationHistoryTableViewCell.self, forCellReuseIdentifier: NotificationHistoryTableViewCell.identifier)
        myTableView.dataSource = self
        myTableView.delegate = self
        myTableView.addSubview(refreshControl)
        myTableView.sectionHeaderHeight = headerSectionHeight
        view.addSubview(myTableView)

        self.title = "Notificações"
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    func reloadNotifications(onFinished: (() -> Void)? = nil) {
        self.notificationService.fetchAllNotifications {
            [weak self] notifications in
            
            if notifications.count > 0 {
                self?.unreadNotifications = notifications.filter({!$0.isRead})
                self?.readNotifications = notifications.filter({$0.isRead})
            } else {
                DispatchQueue.main.async {
                    self?.myTableView.separatorStyle = .none
                    self?.myTableView.backgroundView = self?.emptyLabel
                }
            }
            
            DispatchQueue.main.async {
                [weak self] in
                self?.myTableView.reloadData()
            }
            onFinished?()
        }
    }
    
    @objc func refreshData(_ sender: AnyObject) {
        reloadNotifications {
            [weak self] in
            DispatchQueue.main.async {
                self?.refreshControl.endRefreshing()
            }
        }
    }
}

extension NotificationHistoryListTableViewController: UITableViewDelegate, UITableViewDataSource {
    func hasReadAndUnreadNotifications() -> Bool {
        return unreadNotifications.count > 0 && readNotifications.count > 0
    }
    
    func hasAnyNotification() -> Bool {
        return unreadNotifications.count > 0 || readNotifications.count > 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let notRead = "Não lidas"
        let read = "Lidas"
        
        if hasReadAndUnreadNotifications() {
            return section == 0 ? notRead : read
        } else if hasAnyNotification() {
            return unreadNotifications.count > 0 ? notRead : read
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let readCount = readNotifications.count
        let unreadCount = unreadNotifications.count
        
        if hasReadAndUnreadNotifications() {
            return section == 0 ? unreadCount : readCount
        } else if hasAnyNotification() {
            return readCount > 0 ? readCount : unreadCount
        }
        
        return 0
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections = 0
        
        if unreadNotifications.count > 0 {
            numOfSections += 1
        }

        if readNotifications.count > 0 {
            numOfSections += 1
        }
        
        return numOfSections
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var notification: Notification!
        
        if hasReadAndUnreadNotifications() {
            notification = indexPath.section == 0 ? unreadNotifications[indexPath.row] : readNotifications[indexPath.row]
        } else if hasAnyNotification() {
            if unreadNotifications.count > 0 {
                notification = unreadNotifications[indexPath.row]
            } else {
                notification = readNotifications[indexPath.row]
            }
        }
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: NotificationHistoryTableViewCell.identifier, for: indexPath) as? NotificationHistoryTableViewCell {
            cell.setup(with: notification)
            
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if indexPath.section == 0 && unreadNotifications.count > 0 {
            var unreadNotification = unreadNotifications[indexPath.row]
            unreadNotification.isRead = true
            unreadNotifications.remove(at: indexPath.row)
            readNotifications.append(unreadNotification)
        }
        
        DispatchQueue.main.async {
            [weak self] in
            self?.myTableView.reloadData()
        }
        
        if let cell = tableView.cellForRow(at: indexPath) as? NotificationHistoryTableViewCell {
            cell.setRead()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
}
