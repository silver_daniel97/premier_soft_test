//
//  NotificationHistoryTableViewCell.swift
//  premier_soft_test
//
//  Created by Daniel Silveira on 25/06/21.
//

import Foundation
import UIKit

class NotificationHistoryTableViewCell: UITableViewCell {
    
    static var identifier = "cell"
    
    private var content: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(30)
        return label
    }()
    
    private var dotRead: UIView = {
        let dotView = UIView()

        dotView.backgroundColor = UIColor.systemRed
        dotView.layer.cornerRadius = 5
        return dotView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(dotRead)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.selectionStyle = .none
    }
    
    func setup(with notification: Notification) {
        self.textLabel?.text = notification.content
        self.dotRead.isHidden = notification.isRead
        
        let dotSize: CGFloat = 10
        self.dotRead.frame = CGRect(x: self.contentView.frame.width - dotSize - 16, y: 16, width: dotSize, height: dotSize)
        
        self.textLabel?.font = textLabel?.font.withSize(20)
    }
    
    func setRead() {
        self.dotRead.isHidden = true
    }
}
