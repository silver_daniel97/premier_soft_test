//
//  NotificationModel.swift
//  premier_soft_test
//
//  Created by Daniel Silveira on 24/06/21.
//

import Foundation

struct NotificationContainer: Codable {
    let notifications: [Notification]
}

// MARK: - Notification
struct Notification: Codable {
    let id: String
    var isRead: Bool
    let content: String
}
